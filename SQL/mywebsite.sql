﻿CREATE DATABASE myweb DEFAULT CHARACTER SET utf8;
use myweb;
CREATE TABLE user(  
id  SERIAL PRIMARY KEY UNIQUE AUTO_INCREMENT,
user_id varchar(255) UNIQUE NOT NULL,
user_password varchar(255) NOT NULL,
user_name varchar(255) NOT NULL,
user_createdate date NOT NULL,
user_updatedate date NOT NULL
);
CREATE TABLE recipielist(
recipie_id SERIAL PRIMARY KEY UNIQUE AUTO_INCREMENT,
recipie_name varchar(255) NOT NULL,
user_id int NOT NULL
);
CREATE TABLE recipie_make(
recipie_make_id SERIAL PRIMARY KEY UNIQUE AUTO_INCREMENT,
recipie_id int NOT NULL,
recipie_make varchar(255) NOT NULL
);
CREATE TABLE recipie_ing(
recipie_ing_id SERIAL PRIMARY KEY UNIQUE AUTO_INCREMENT,
recipie_id int NOT NULL,
recipie_ing varchar(255)
);
INSERT INTO user(user_Id,user_password,user_name,user_createdate,user_updatedate) VALUE('admin','aaa','admin',NOW(),NOW());
