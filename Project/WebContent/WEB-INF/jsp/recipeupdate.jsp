<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レシピ編集</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/recipieadd.css" rel="stylesheet">


</head>
<body>
	<h1>レシピ追加</h1>
	<h3>メニュー名</h3>
	<form action="RecipeUpdateServlet" method="post">
	<input name ="id" value="${recipeList.recipeId}"type="hidden">
	<div class="a">
		<input type="text" class="form-control" value="${recipeName}" name="name">
	</div>
	<h3>材料名</h3>
	<c:forEach var="recipe" items="${recipeIng}" varStatus="status">
		<div id="input_pluralBox">
			<div id="input_plural">
				<input type="text" class="form-control" value="${recipe.recipeIng}" name="ing">
				<input type="button" value="＋" class="add pluralBtn"> <input
					type="button" value="－" class="del pluralBtn">
			</div>
		</div>
	</c:forEach>
	<h3>作り方</h3>
	<c:forEach var="recipe" items="${recipeMake}">
		<div id="input_pluralBox">
			<div id="input_plural">
				<input type="text" class="form-control" value="${recipe.recipeMake}"name="make">
				<input type="button" value="＋" class="add pluralBtn"> <input
					type="button" value="－" class="del pluralBtn">
			</div>
		</div>
	</c:forEach>
	<button type="submit" class="btn btn-primary">追加</button>
	</form>
	<div class="aa">
		<a href="RecipeMenuServlet"id="${recipeName}">戻る</a>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="css/recipieadd.js">

	</script>
</body>
</html>