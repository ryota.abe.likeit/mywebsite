<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/userinf.css" rel="stylesheet">
</head>
<body>
	<div class="logout">
		<div class="row col-3 aaa">
			<h3>${userInfo.name}</h3>
			<a href="LogoutServlet">
				<h2 class="ml-5">ログアウト</h2>
			</a>
		</div>
	</div>
	<h1>ユーザ情報</h1>
	<p></p>
	<div class="userInf">
		<pre>
       <a>ログインID:  ${userInfo.loginId}</a>
        <p></p>
       <a>ユーザ名  :  ${userInfo.name}</a>
        </pre>
	</div>
	<div class=" button">

		<a href="RecipeListServlet" type="button" class="btn btn-secondary btn-lg">戻る</a>
		&nbsp;
		<a href="UserupdateServlet" type="button" class="btn btn-primary btn-lg">更新</a>
		&nbsp;
		<a href="UserdeleteServlet" type="button" class="btn btn-danger btn-lg">削除</a>
	</div>
</body>
</html>