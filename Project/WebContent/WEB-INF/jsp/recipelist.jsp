<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レシピ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/recipielist.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
</head>
<body>
	<h1>レシピ一覧</h1>
	<a href="UserMenuServlet">ユーザ情報</a>
	<h5>検索</h5>
	<form class="form-inline" action="RecipeSerchServlet">
		<div class="form-group mx-sm-3 mb-2">
			<label for="inputword2" class="sr-only"></label> <input type="text"
				class="form-control" id="inputword" placeholder="検索ワード" name="word">
		</div>
		<button type="submit" class="btn btn-primary mb-2">検索</button>
	</form>
	<p></p>
	<div class="add">
		<a href="RecipeAddServlet" type="submit" class="btn btn-primary ">追加</a>
	</div>
	<div class="dropdown mx-sm-3 mb-2">
		<button class="btn btn-primary dropdown-toggle" type="button"
			id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
			aria-expanded="false">並び替え</button>
			<form action="RecipeListServlet" method="post">
		<div class="dropdown-menu" aria-labelledby="dropdownMenu2">

			<button class="dropdown-item" type="submit"value="a" name="c">昇順</button>

			<button class="dropdown-item" type="submit"value="b"name="c">降順</button>

		</div>
		</form>
		<c:forEach var="recipe" items="${recipeList}">
		<td>
			<div class="list-group">

				<a href="RecipeMenuServlet?recipeId=${recipe.recipeId}"
					class="list-group-item list-group-item-action">${recipe.recipeName}</a>
			</div>
			</td>
		</c:forEach>
	</div>
</body>
</html>