<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>メニュー選択</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/recipiemenu.css" rel="stylesheet">

</head>
<body>
	<h1>レシピ名 ${recipeList.recipeName}</h1>
	<div class="bottons">
		<a href="RecipeInfServlet?id=${recipeList.recipeId}" type="button"
			class="btn btn-primary btn-lg">詳細</a> <a
			href="RecipeUpdateServlet?id=${recipeList.recipeId}" type="button"
			class="btn btn-success btn-lg">編集</a> <a
			href="RecipeDeleteServlet?id=${recipeList.recipeId}" type="button"
			class="btn btn-danger btn-lg">削除</a>
	</div>
	<a href="RecipeListServlet">戻る</a>
</body>
</html>