<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/userdelete.css" rel="stylesheet">
</head>
<body>
	<h1>ユーザ削除</h1>
	<p></p>
	<h4>ユーザ名 ${userInfo.name}を本当に削除してもよろしいですか</h4>
	<p></p>
	<div class=" botton">
		<a href="UserMenuServlet" type="button" class="btn btn-primary">キャンセル</a>
		&nbsp;
		<form action="UserdeleteServlet" method="post">
			<button type="submit" class="btn btn-danger">OK
			</button>
		</form>
	</div>
</body>
</html>