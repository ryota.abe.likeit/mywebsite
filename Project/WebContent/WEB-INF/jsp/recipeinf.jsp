<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レシピ詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/recipieinf.css" rel="stylesheet">
</head>
<body>

	<div class="col-7 mx-auto">
		<div class="a">
			<h1 class="col-8 mx-auto">${recipeName}</h1>
		</div>

		<div id="bc_container" class="row">
			<div class="b col-6">
				<ul>
					<c:forEach var="recipe" items="${recipeIng}" varStatus="status">
						<li>${recipe.recipeIng}</li>

						<c:if test="${(status.index + 1) % 15 == 0}">
				</ul>
			</div>
			<div class="c col-6">
				<ul class="aaa">
					</c:if>

					</c:forEach>
				</ul>
			</div>
		</div>
		<div class="d">
			<ol start="1">
				<c:forEach var="recipe" items="${recipeMake}">
					<li>${recipe.recipeMake}</li>
				</c:forEach>
			</ol>
		</div>
	</div>
	<div class="link">
		<a href="RecipeMenuServlet">戻る</a>
	</div>
</body>
</html>