<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet"  	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"           integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/signup.css" rel="stylesheet">
</head>
<body>
   <h1>ユーザ情報更新</h1>
   <c:if test="${errMsg !=null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
   <form action="UserupdateServlet" method="post">
      <div class="form-group">
    <label for="exampleInputLoginId">ログインID</label>
    <input type="text" class="form-control" id="InputLoginId" value ="${userInfo.loginId}" name="loginId">
    <label for="exampleInputUserName">ユーザ名</label>
    <input type="text" class="form-control" id="InputUserName" value ="${userInfo.name }"name="name">
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">パスワード</label>
    <input type="password" class="form-control" id="InputPassword1" placeholder="Password"name="passwordA">
    <label for="exampleInputPassword2">パスワード(確認)</label>
    <input type="password" class="form-control" id="InputPassword2" placeholder="Password"name="passwordB">
  </div>
    <button type="submit" class="btn btn-primary">更新</button>
    </form>
    <div class="a">
    <a href="UserMenuServlet">戻る</a>
        </div>
</body>
</html>