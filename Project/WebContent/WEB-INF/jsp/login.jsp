<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>
<body>
	<h1>ログイン</h1>
	<p>
		<a href="SignupServlet">新規登録</a>
	</p>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<div class="a">
		<form action="LoginServlet" method="post">
			<div class="form-group">
				<label for="exampleInputLoginId">ログインID</label> <input type="text"
					class="form-control" id="InputLoginId" placeholder="ログインID"
					name="loginId">

			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label> <input
					type="password" class="form-control" id="InputPassword1"
					placeholder="Password" name="password">
			</div>

			<button type="submit" class="btn btn-primary">ログイン</button>
		</form>
	</div>

</body>
</html>
