<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レシピ削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/recipiedelete.css" rel="stylesheet">
</head>
<body>

	<h4>レシピ名 ${recipeList.recipeName}を本当に削除してもよろしいですか</h4>
	<p>
	<div class="button">
		<a href="RecipeMenuServlet" type="button" class="btn btn-primary">キャンセル</a>
		&nbsp;
		<form action="RecipeDeleteServlet" method="post" >
		<input name ="id" value="${recipeList.recipeId}"type="hidden">
			<button type="submit" class="btn btn-danger">OK</button>
		</form>
	</div>
</body>
</html>