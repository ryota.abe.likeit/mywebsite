package model;

import java.io.Serializable;

public class RecipeMake implements Serializable {
	private int recipeId;
	private int recipeMakeId;
	private String recipeMake;

	public RecipeMake(String recipeMake) {
		this.recipeMake = recipeMake;
	}

	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public int getRecipeMakeId() {
		return recipeMakeId;
	}

	public void setRecipeMakeId(int recipeMakeId) {
		this.recipeMakeId = recipeMakeId;
	}

	public String getRecipeMake() {
		return recipeMake;
	}

	public void setRecipeMake(String recipeMake) {
		this.recipeMake = recipeMake;
	}
}
