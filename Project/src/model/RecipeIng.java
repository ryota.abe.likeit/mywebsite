package model;

import java.io.Serializable;

public class RecipeIng implements Serializable {

	private int recipeIngId;
	private int recipeId;
	private String recipeIng;

















	public RecipeIng(String recipeIng) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.recipeIng = recipeIng;
	}

	public int getRecipeIngId() {
		return recipeIngId;
	}

	public void setRecipeIngId(int recipeIngId) {
		this.recipeIngId = recipeIngId;
	}

	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipeId) {
		this.recipeId = recipeId;
	}

	public String getRecipeIng() {
		return recipeIng;
	}

	public void setRecipeIng(String recipeIng) {
		this.recipeIng = recipeIng;
	}
}
