package model;

import java.io.Serializable;

public class RecipeList implements Serializable {
	private int recipeId;
	private String recipeName;
	private int userId;

	public RecipeList(String recipeName, int recipeId) {
		this.recipeId = recipeId;
		this.recipeName = recipeName;
	}


	


	public int getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(int recipieId) {
		this.recipeId = recipieId;
	}

	public String getRecipeName() {
		return recipeName;
	}

	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
