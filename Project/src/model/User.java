package model;

import java.io.Serializable;

public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private String password;
	private String createDate;
	private String updateDate;

	public User(String loginId, String name,int id) {
		this.loginId = loginId;
		this.name = name;
		this.id = id;
	}

	public void password(String password) {
		this.password = password;
	}


	public User(int idP, String loginIdData, String nameData, String password) {
		this.id = idP;
		this.loginId = loginIdData;
		this.name = nameData;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}



}
