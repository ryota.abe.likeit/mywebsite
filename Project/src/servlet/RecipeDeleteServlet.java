package servlet;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RecipeDao;
import model.RecipeList;

/**
 * Servlet implementation class RecipeDeleteServlet
 */
@WebServlet("/RecipeDeleteServlet")
public class RecipeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecipeDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		RecipeDao recipedao = new RecipeDao();
		RecipeList recipelist= recipedao.findByRecipeId(id);
		 request.setAttribute("recipeList", recipelist);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipedelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		RecipeDao recipedao = new RecipeDao();

		recipedao.deleteRecipe(id);

		response.sendRedirect("RecipeListServlet");

	}

}
