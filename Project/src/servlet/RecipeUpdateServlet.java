package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RecipeDao;
import model.RecipeIng;
import model.RecipeList;
import model.RecipeMake;

/**
 * Servlet implementation class RecipeUpdateServlet
 */
@WebServlet("/RecipeUpdateServlet")
public class RecipeUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RecipeUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		RecipeDao recipedao = new RecipeDao();

		String recipeName = recipedao.findByRecipeName(id);
		List<RecipeIng> recipeIng = recipedao.findIng(id);
		List<RecipeMake> recipeMake = recipedao.findMake(id);
		RecipeList recipelist = recipedao.findByRecipeId(id);

		request.setAttribute("recipeList", recipelist);
		request.setAttribute("recipeName", recipeName);
		request.setAttribute("recipeIng", recipeIng);
		request.setAttribute("recipeMake", recipeMake);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		RecipeDao recipedao = new RecipeDao();

		//メニュー名登録
		String recipeName = request.getParameter("name");
		int recipeId;
		
			recipeId = recipedao.nameupdate(recipeName, id);

			//材料更新
			String[] ingList = request.getParameterValues("ing");
			//レシピの材料を一度すべて消す
			recipedao.ingdelete(recipeId);
			//材料を更新する
			for (String ing : ingList) {
				recipedao.ingupdate(ing, recipeId);
			}
			//作り方更新
			String[] makeList = request.getParameterValues("make");
			//レシピの作り方を一度すべて消す
			recipedao.makedelete(recipeId);
			//作り方を更新する
			for (String make : makeList) {
				recipedao.makeupdate(make, recipeId);
			}
		
		response.sendRedirect("RecipeListServlet");
	}
}
