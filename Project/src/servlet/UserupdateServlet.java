package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserupdateServlet
 */
@WebServlet("/UserupdateServlet")
public class UserupdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserupdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		 dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String passwordA = request.getParameter("passwordA");
		String passwordB = request.getParameter("passwordB");
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		int id = user.getId();

		UserDao userdao = new UserDao();
		if (!passwordA.equals(passwordB)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("userB",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
		userdao.updateUser(loginId,name,passwordA,id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userinf.jsp");
		 dispatcher.forward(request, response);
	}
	}

}
