package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.RecipeDao;
import model.RecipeList;
import model.User;

/**
 * Servlet implementation class RecipeSerchServlet
 */
@WebServlet("/RecipeSerchServlet")
public class RecipeSerchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecipeSerchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String word  = request.getParameter("word");
		RecipeDao recipedao = new RecipeDao();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		int userId = user.getId();
		if(word.equals(null)) {
			 List<RecipeList> recipeList =recipedao.findByRecipeListAll(userId);
			 request.setAttribute("recipeList", recipeList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipelist.jsp");
			dispatcher.forward(request, response);
		}else {
			List<RecipeList> recipeList = recipedao.serchRecipe(userId,word);
			request.setAttribute("recipeList", recipeList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipelist.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
