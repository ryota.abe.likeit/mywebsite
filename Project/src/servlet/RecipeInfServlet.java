package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.RecipeDao;
import model.RecipeIng;
import model.RecipeMake;

/**
 * Servlet implementation class RecipieInfServlet
 */
@WebServlet("/RecipeInfServlet")
public class RecipeInfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RecipeInfServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		RecipeDao recipedao = new RecipeDao();
		String recipeName = recipedao.findByRecipeName(id);
		List<RecipeIng> recipeIng = recipedao.findIng(id);
		List<RecipeMake> recipeMake = recipedao.findMake(id);

		request.setAttribute("recipeName", recipeName);
		request.setAttribute("recipeIng", recipeIng);
		request.setAttribute("recipeMake", recipeMake);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeinf.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
