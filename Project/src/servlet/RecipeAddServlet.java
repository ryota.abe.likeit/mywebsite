package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.RecipeDao;
import model.User;

/**
 * Servlet implementation class RecipeAddServlet
 */
@WebServlet("/RecipeAddServlet")
public class RecipeAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecipeAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/recipeadd.jsp");
		 dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		int id = user.getId();
		RecipeDao recipedao = new RecipeDao();

		//メニュー名登録
		String recipeName = request.getParameter("name");
		int recipeId;
		try {
			recipeId = recipedao.nameAdd(recipeName,id);


		//材料登録
		String[] ingList = request.getParameterValues("ing");
		for(String ing: ingList){
		recipedao.ingAdd(ing,recipeId);
		}
		//作り方登録
		String[] makeList = request.getParameterValues("make");
		for(String make: makeList){
		recipedao.makeAdd(make,recipeId);
		}
		} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		response.sendRedirect("RecipeListServlet");
	}

	}

