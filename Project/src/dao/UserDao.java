package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE user_id = ? and user_password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			String loginIdData = rs.getString("user_id");
			String nameData = rs.getString("user_name");
			return new User(loginIdData, nameData, id);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void signup(String loginId, String passwordA, String name) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(user_id,user_password,user_name,user_createdate,user_updatedate) VALUE(?,?,?,NOW(),NOW())";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			String ps = this.password(passwordA);
			pstmt.setString(1, loginId);
			pstmt.setString(2, ps);
			pstmt.setString(3, name);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findById(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idP = rs.getInt("id");
			String loginIdData = rs.getString("user_id");
			String nameData = rs.getString("user_name");
			String password = rs.getString("user_password");

			return new User(idP, loginIdData, nameData, password);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void updateUser(String loginId, String name, String passwordA, int id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update user set user_id =?, user_password =?, user_name = ? where id  = ?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			String ps = this.password(passwordA);
			pstmt.setString(1, loginId);
			pstmt.setString(2, ps);
			pstmt.setString(3, name);
			pstmt.setInt(4, id);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void deleteUser(int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "delete from  user  where id  = ?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, id);
			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findByLoginId(String loginId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE user_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idP = rs.getInt("id");
			String loginIdData = rs.getString("user_id");
			String nameData = rs.getString("user_name");

			String password = rs.getString("user_password");

			return new User(idP, loginIdData, nameData, password);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String password(String passwordA) {
		String source = passwordA;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}
}