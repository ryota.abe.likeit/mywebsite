package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.RecipeIng;
import model.RecipeList;
import model.RecipeMake;

public class RecipeDao {
	public RecipeList findByRecipeId(String recipeid) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where recipie_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, recipeid);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String nameData = rs.getString("recipie_name");
			int recipeId = rs.getInt("recipie_id");
			return new RecipeList(nameData, recipeId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public List<RecipeList> findByRecipeListAll(int id) {
		Connection conn = null;
		List<RecipeList> recipeList = new ArrayList<RecipeList>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where user_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int recipeId = rs.getInt("recipie_id");
				String recipeName = rs.getString("recipie_name");

				RecipeList List = new RecipeList(recipeName, recipeId);

				recipeList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeList;
	}

	public String findByRecipeName(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where recipie_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String nameData = rs.getString("recipie_name");

			return nameData;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public List<RecipeIng> findIng(String id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		List<RecipeIng> recipeIngList = new ArrayList<RecipeIng>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipie_ing where recipie_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				String recipeIng = rs.getString("recipie_ing");

				RecipeIng List = new RecipeIng(recipeIng);

				recipeIngList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeIngList;
	}

	public List<RecipeMake> findMake(String id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		List<RecipeMake> recipeMakeList = new ArrayList<RecipeMake>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipie_make where recipie_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				String recipeMake = rs.getString("recipie_make");
				RecipeMake List = new RecipeMake(recipeMake);

				recipeMakeList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeMakeList;
	}

	public int nameAdd(String recipeName, int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO recipielist(recipie_name,user_id) VALUES(?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setString(1, recipeName);
			st.setInt(2, id);
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public void ingAdd(String ing, int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO recipie_ing(recipie_ing,recipie_id) VALUE(?,?)";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, ing);
			pstmt.setInt(2, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void makeAdd(String make, int recipeId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO recipie_make(recipie_make,recipie_id) VALUE(?,?)";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, make);
			pstmt.setInt(2, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void deleteRecipe(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "delete from  recipielist  where recipie_id  = ?;";
			String sql2 = "delete from  recipie_make  where recipie_id  = ?;";
			String sql3 = "delete from  recipie_ing  where  recipie_id  = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			PreparedStatement pstmt3 = conn.prepareStatement(sql3);

			pstmt.setString(1, id);
			pstmt2.setString(1, id);
			pstmt3.setString(1, id);
			int rs = pstmt.executeUpdate();
			int rs2 = pstmt2.executeUpdate();
			int rs3 = pstmt3.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public int nameupdate(String recipeName, String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "update recipielist set recipie_name =? where recipie_id =?;";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, recipeName);
			pstmt.setString(2, id);
			int rs = pstmt.executeUpdate();

			String sql2 = "SELECT * FROM recipielist where recipie_id=?";

			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1, id);
			ResultSet rs2 = pStmt2.executeQuery();
			if(!rs2.next()) {
				return 0;
			}
			int recipeId = rs2.getInt("recipie_id");
			return recipeId;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}

	public void ingdelete(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();


			String sql= "delete from  recipie_ing  where  recipie_id  = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);


			pstmt.setInt(1, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void makedelete(int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();


			String sql= "delete from  recipie_make  where  recipie_id  = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);


			pstmt.setInt(1, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void ingupdate(String ing, int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO recipie_ing(recipie_ing,recipie_id) VALUE(?,?)";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, ing);
			pstmt.setInt(2, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void makeupdate(String make, int recipeId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "INSERT INTO recipie_make(recipie_make,recipie_id) VALUE(?,?)";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, make);
			pstmt.setInt(2, recipeId);

			int rs = pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<RecipeList> sortRecipeA(int userId) {
		Connection conn = null;
		List<RecipeList> recipeList = new ArrayList<RecipeList>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where user_id=? order by recipie_name asc";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int recipeId = rs.getInt("recipie_id");
				String recipeName = rs.getString("recipie_name");

				RecipeList List = new RecipeList(recipeName, recipeId);

				recipeList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeList;
	}

	public List<RecipeList> sortrecipeB(int userId) {
		Connection conn = null;
		List<RecipeList> recipeList = new ArrayList<RecipeList>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where user_id=? order by recipie_name desc";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int recipeId = rs.getInt("recipie_id");
				String recipeName = rs.getString("recipie_name");

				RecipeList List = new RecipeList(recipeName, recipeId);

				recipeList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeList;
	}

	public List<RecipeList> serchRecipe(int userId, String word) {
		Connection conn = null;
		List<RecipeList> recipeList = new ArrayList<RecipeList>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM recipielist where user_id=? and recipie_name like ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);
			pStmt.setString(2, "%"+word+"%");
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int recipeId = rs.getInt("recipie_id");
				String recipeName = rs.getString("recipie_name");

				RecipeList List = new RecipeList(recipeName, recipeId);

				recipeList.add(List);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return recipeList;
	}
}






